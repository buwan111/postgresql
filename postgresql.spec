%define debug_package %{nil}
Name: postgresql
Version: 13.4
Release: 1%{?dist}
Summary: PostgreSQL Database Server
License: PostgreSQL
Group: Applications/Databases
URL: https://www.postgresql.org
Source0: postgresql-13.4.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-build
BuildRequires: readline-devel, zlib-devel, gcc, make
Requires: readline, zlib

%description
PostgreSQL is a powerful, open-source object-relational database system.

%prep
%setup -q

%build
# Configure with standard options
./configure --prefix=/usr/local/pgsql
make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%pre
# Add PostgreSQL user if it does not exist
getent group postgres >/dev/null || groupadd -r postgres
getent passwd postgres >/dev/null || useradd -r -g postgres -s /bin/bash postgres

%post
# Set up data directory with correct permissions
chown -R postgres:postgres /usr/local/pgsql

%files
%defattr(-,postgres,postgres,-)
/usr/local/pgsql

%clean
rm -rf %{buildroot}

%changelog
* Mon Oct 09 2023 Buwan <buwan> - 13.4-1  
- Build of Postgresql 13.4.
