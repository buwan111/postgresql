# 在openeueler22.03上适配PostgreSQL

## 介绍
PostgreSQL是一种功能非常强大的、源代码开放的客户/服务器关系型数据库管理系统（RDBMS），也可以被看作是一个对象-关系型数据库管理系统（ORDBMS）。

## rpm包二进制安装
rpm二进制包由源码包通过spec文件构建而成

1.安装rpm包
```
rpm -ivh postgresql-13.4-1.aarch64.rpm
```
2. 切换到postgres用户
```
su - postgres
```
3. 初始化数据库
```
/usr/local/pgsql/bin/initdb -D /usr/local/pgsql/data
```
4. 启动数据库
```
/usr/local/pgsql/bin/pg_ctl -D /usr/local/pgsql/data -l logfile start
```
5. 创建test数据库
```
/usr/local/pgsql/bin/createdb test
```
6. 连接test数据库
```
/usr/local/pgsql/bin/psql test
```

## 源码安装
https://ftp.postgresql.org/pub/source/v13.4/postgresql-13.4.tar.gz

1. 下载并解压源码包
```
wget https://ftp.postgresql.org/pub/source/v13.4/postgresql-13.4.tar.gz
tar -xvf postgresql-13.4.tar.gz
cd postgresql-13.4
```
2. 安装依赖
```
yum install readline-devel zlib-devel
```
3. 配置

```
./configure   
```
4. 编译
```
make
```
5. 安装
```
make install
```
6. 添加postgres用户
```
adduser postgres   
```
7. 创建data存放目录
```
mkdir /usr/local/pgsql/data 
```
8. 给postgres用户授权
```
chown postgres /usr/local/pgsql/data
```
9. 切换到postgres用户
```
su - postgres
```
10. 初始化数据库
```
/usr/local/pgsql/bin/initdb -D /usr/local/pgsql/data
```
11. 启动数据库
```
/usr/local/pgsql/bin/pg_ctl -D /usr/local/pgsql/data -l logfile start
```
12. 创建test数据库
```
/usr/local/pgsql/bin/createdb test
```
13. 连接test数据库
```
/usr/local/pgsql/bin/psql test
```


#### 参考
https://www.postgresql.org/docs/current/install-make.html#INSTALL-SHORT-MAKE1. 这里是列表文本